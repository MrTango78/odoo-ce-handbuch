# Projekt

Mit der Projektverwaltung von Odoo, können Sie Projekte und einzelne Aufgaben erfassen.
Zu jeder Aufgabe können bei Bedarf auch Zeiten erfasst und abgerechnet werden.



## Erfassen von Zeiten in Projektaufgaben

Das Erstellen von Projekten und Aufgaben ist selbsterklärend.

![Screenshot: Projekt mit Aufgabe in Bearbeitung](static/odoo_projekt_mit_aufgabe.png)

Wenn wir jetzt in eine Aufgabe hinein wechseln und die bearbeiten, können wir dort im Reiter Zeiterfassung unsere Zeiten erfassen.
![Screenshot: Zeiten in einer Projektaufgabe erfassen](static/odoo_projekt_zeiten_für_aufgabe_erfassen.png)

Nun gehen wir in der Projektübersicht, bei unserem Projekt auf Bearbeiten.

![Screenshot: Projekt-bearbeiten-Menü](static/odoo_projekt_bearbeiten_menu.png)

Hier können wir nun unter anderem dem Projekt einem Kunden zuordnen.

![Screenshot: Projekt Kunden zuordnen](static/odoo_projekt_bearbeiten_kunden_zuordnen.png)

Damit wir die erfassten Zeiten abrechnen können, brauchen wir ein spezielles Dienstleistungsprodukt, welches wir nun einrichten.

Hier für gehen wir in Verkäufe >> Produkte >> Produkte und erstellen ein Produkt mit dem namen Support.

![](static/odoo_dienstleistungs_produkt_fuer_zeiterfassung.png)

Im Reiter Verkäufe des Produktes können  wir nun Einstellungen für die Abrechnung von Zeiten vornehmen.

Wählen Sie hier:

- Stundenzettel für Aufgaben
- Aufgabe nicht erstellen


![](static/odoo_dienstleistungs_produkt_fuer_zeiterfassung_abrechnung.png)



Anschließend gehen wir in der Projektübersicht, bei unserem Projekt auf Übersicht.

![Screenshot: Projekt-Übersicht-Menü](static/odoo_projekt_uebersicht_menu.png)

Hier sehen wir nun eine Übersicht der erfassten Zeiten und können von hier aus die Zeiten in Rechnung stellen. Bevor dies tun können, müssen wir allerdings einen Auftrag (Sales Order) für dieses Projekt anlegen. Dies läßt sich über den Knopf oben links ganz einfach machen.

![Screenshot: Projektübersicht mit erfassten Zeiten](static/odoo_projekt_uebersicht01.png)

Hier können wir nun eine Dienstleistung (Produkt) auswählen, welches für diesen Zweck vorbereit wurde.

![Screenshot: Projektauftrag erzeugen](static/odoo_projekt_auftrag_anlegen.png)

Im dem erzeugten Projektauftrag sehen wir nun eine Auftragsposition mit der zuvorausgewählten Dienstleistung (Produkt). Wir können nun eine Rechnung üben der entsprechenden Knopf oben links erzeugen.

![Screenshot: Projektauftrag bearbeiten](static/odoo_projekt_auftrag_bearbeiten.png)

Hier wählen wir Reguläre Rechnung erzeugen und klicken auf "Erzeuge und zeige Rechnung an".

![Screenshot: Projektrechnung erzeugen](static/odoo_projekt_auftrag_bearbeiten_rechnung_anlegen.png)

Wir haben nun eine erzeugte Rechnung vor uns und sehen dort als Rechnungsposition unsere abzurechnenden Stunden. Wenn Sie das OCA Modul account-invoicing/sale_timesheet_invoice_description installier haben, werden Ihnen dort auch die einzelnen Zeiten in der Beschreibung aufgelistet.

![Screenshot: Projektrechnung](static/odoo_projekt_rechnung.png)

