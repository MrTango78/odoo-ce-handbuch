# Benutzerdefinierte Stards

Um bei neuen Kontakten (Partnern) z.B. als Standardauswahl für das Land, Deutschland vorausgewählt zu haben, können wir unter Benutzerdefinierte Standards eine Vorauswahl einstellen.

Wir gehen auf: Einstellungen > Benutzerdefinierte Stards

![](static/odoo_benutzerdefinierte-standards-setzen.png)

Hier erstellen oder ändern wir einen Eintrag für Land (res.partner)

![](static/odoo_benutzerdefinierte-standards-setzen_land-deutschland.png)

Von nun an sollte Deutschland bei neuen Kontakten vorausgewählt sein.

