# Zeiterfassung

Mit der Zeiterfassung von Odoo, können Sie Ihre projektbezogenen Zeiten erfassen und diese anschließend abrechnen.

Eine detaillierte Beschreibung zum erfassen von Zeiten in Projekten und die anschließende Abrechnung dieser, finden Sie [hier unter Projekt](projekt.md).

![](static/odoo_zeiterfassung.png)

Auch der Import per CSV-Datei lässt sich gut nutzen um erfasste Zeiten aus anderen Anwendungen zu importieren. Nach dem Import muss man eventuell nur noch die Einträge Aufgaben zuordnen oder diese direkt aus der Liste heraus anlegen.
