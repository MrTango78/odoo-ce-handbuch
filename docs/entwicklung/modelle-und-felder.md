# Modelle und Felder

## many2one Feld

Im folgenden Beispiel fügen wir ein Feld zum Partner (Kontakt) Modell hinzu, mit welchem wir auf Kontakte verweisen können, deren Typ "landkreis" entspricht. Weiterhin wird der Standardtyp auch als context gesetz, damit durch das Feld neu erzeugte Kontakte den richtigen Typ gesetz haben.

```
class PartnerLandkreisRelation(models.Model):
    _inherit = "res.partner"
    landkreis = fields.Many2one(
        "res.partner",
        string="Landkreis",
        context={"default_type": "landkreis"},
        domain="[('type','=','landkreis')]",
    )
```

![](static/odoo_many2one.png)