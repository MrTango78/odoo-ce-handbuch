# Das Odoo-Community-Edition-Handbuch

Dies ist eine Sammlung von nützlichen Anleitungen für die Odoo Community Edition.
Aktuell beziehen sich die Beispiele auf Odoo 13/14.

![Screenshot: Odoo CE Hauptmenü](static/odoo_ce_main_menu.png)

Von Herzen gern bereitgestellt von [Derico](https://derico.de).
